﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    public float runSpeed = 8f;
    public Joystick joystick;
    public Animator animator;

    void Update()
    {
        animator.SetBool("Attack", false);
        if (joystick.Horizontal < 0)
        {
            transform.localRotation = Quaternion.Euler(0, 180, 0);
        }
        else if (joystick.Horizontal > 0)
        {
            transform.localRotation = Quaternion.Euler(0, 0, 0);
        }

        Vector3 movement = new Vector3(joystick.Horizontal, 0f, 0f);
        animator.SetFloat("Speed", Mathf.Abs(joystick.Horizontal));
        transform.position += Time.deltaTime * runSpeed * movement;

    }

    void FixedUpdate()
    {
    }
}

