﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class SaveLoadUser : MonoBehaviour
{
    public void Save(InputField Field)
    {
        PlayerPrefs.SetString("user", Field.text);
    }

    public void Load(Text TextField)
    {
        TextField.text += PlayerPrefs.GetString("user");
    }
}
