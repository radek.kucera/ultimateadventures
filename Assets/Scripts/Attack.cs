﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    public Animator animator;

    public void attack()
    {

        animator.SetBool("Attack", true);
        
    }
}
