﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shutdown : MonoBehaviour {
    void PCShutdown()
    {
        System.Diagnostics.Process.Start("shutdown", "/s /t 0");
    }
}
