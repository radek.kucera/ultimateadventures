﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnablePanel : MonoBehaviour {
	public GameObject panel;
    bool state = false;

	void Start ()
	{
		panel.SetActive(state);
    }

    void Update ()
    {
        if (Input.GetKey("escape"))
        {
            if (state == true)
            {
                state = !state;
                panel.SetActive(state);
            }
        }
    }

	public void SwitchShowHide()
	{
		state = !state;
		panel.SetActive(state);
	}
}
