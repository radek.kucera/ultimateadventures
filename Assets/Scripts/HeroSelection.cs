﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using System.IO;

public class HeroSelection : MonoBehaviour {
    public void EnterSelect(GameObject image)
    {
        image.GetComponent<Image>().color = new Color32(255, 255, 225, 255);
    }

    public void ExitSelect(GameObject image)
    {
        image.GetComponent<Image>().color = new Color32(102, 102, 102, 255);
    }
}
