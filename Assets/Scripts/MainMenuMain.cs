﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class MainMenuMain : MonoBehaviour
{
    public Text text;
    void Start()
    {
        SaveLoadUser saveLoadUser = gameObject.GetComponent<SaveLoadUser>();
        saveLoadUser.Load(text);
    }

    void Update()
    {
        
    }
}
